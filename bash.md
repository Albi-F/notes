# Bash

## History Search

Type `Ctrl + R` at the command line and start typing the command.

- `Ctrl + R` --> next match
- `Ctrl + S` --> previous match
- `Ctrl + G` --> abort
- `Enter` --> accept match

## Shortcuts

- `Ctrl + U` --> clear the current line

### Move the cursor

- `Ctrl + A` --> go to the beginning of the line
- `Ctrl + E` --> go to the end of the line

## Commands

- `clear` or `ctrl + l` --> clear all text on the screen and display a new prompt
- `reset` --> reinitialize the terminal session you are in, purging all data
- `history` --> prints the command history
    - `-c` --> use this flag to clear the history
    - `<num>` --> print only the last `<num>` commands
- `!<num>` --> executes the `<num>` command from the bash history
- `!!` --> executes the last command
  - It is particularly useful in case you forgot to use sudo: `sudo !!`

## Streams

In Bash and other Linux shells, when a program is executed, it uses three standard I/O streams:

- `0` --> `stdin`, the standard input stream
- `1` --> `stdout`, the standard output stream
- `2` --> `stderr`, the standard error stream

The input stream provides information to the program, generally by typing in the keyboard.

The program output goes to the standard output stream and the error messages goes to the standard error stream. By default, both input and error streams are printed on the screen.

### Redirection

Redirection is a way to capture the output from a program and send it as input to another program or file. Streams can be redirected using the `n>` operator, where `n` is the stream number. When `n` is omitted, it defaults to 1, the standard output stream.

```bash
command 1> file
```

It is possible to redirect both `stdout` and `stderr` to the same file using `&>`:

```bash
command &> file
```

## Managing processes

- `Ctrl + C` --> terminate the current command
- `Ctrl + Z` --> stop the current command (it can be resumed)

### Jobs

The `jobs` command is used when managing processes that have been started in the background or have been stopped.

### Bg & Fg

The `bg` command is used to run a job/process in the background after it has been stopped or paused. It is start a process directly in the background appending `&` to the end of the command.

The `fg` command brings a background job to the foreground, giving you direct control over the process.

#### Parameters

- `%n` --> `n` is the job number

Both commands operate on the current job if no parameter is provided.

#### Examples

**Bring the job listed as second to the foreground**
```bash
fg %2
```

**Start sleep as a background process**
```bash
sleep 1m &
```

## Other

- `$?` --> variable containing the last exit code

what does `read` do? How to use it in loops?

## Complex Examples

**Remove the comments on top of a file, format it into column, find the number of occurences of each word in a column and print the file into sorted columns**

```bash
tail -n +28 "pi.pfam" | tr -d "#" | column -t | awk '{if ($8=="Repeat") {repeats[$7]++}} END {for (key in repeats) print key, repeats[key]}' | column -t -R 2 | sort -k 2gr
```

**Read length of a fastq file**
```bash
cat ref1.fastq | head -2 | tail -1 | wc -cs
```

**Example of a loop**
```bash
ls * fastq | grep -v fh1.fastq | \
  while read file; do fastqc --threads 6 $file --outdir fastQC_raw; \
  echo $file processed;
  done
```

**Another loop**
```bash
for reads in *.sam; do samtools view -@ $THREADS -bS $reads | samtools sort -@ $THREADS -o ${reads%.sam}.sorted.bam; samtools index -@ $THREADS ${reads%.sam}.sorted.bam; done
```

**Another loop**
```bash
for file in 2_trimming/*_1P.fastq; do bn=$(echo $file | sed "s/_1P.fastq//" | sed s"/2_trimming\///"); pandaseq -f 2_trimming/${bn}_1P.fastq  -r 2_trimming/${bn}_2P.fastq -g 3_mergereads/${bn}.log -u 3_mergereads/${bn}.unaligned.fasta -w 3_mergereads/${bn}.fastq -F; done
```