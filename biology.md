# Biology

## Homology: Orthologs and Paralogs

Homologous genes become separated in evolution in two different ways: separation of two populations with the ancestral gene into two species or gene duplication of the ancestral gene within a lineage.

- Genes separated by speciation are called **orthologs**.
- Genes separated by gene duplication events are called **paralogs**.

## Exon

An exon is any part of a gene that will form a part of the final mature RNA produced by that gene after introns have been removed by RNA splicing.

## Intron

An intron is any nucleotide sequence within a gene that is not expressed or operative in the final RNA product.

## Haploid

Haploid refers to the presence of a single set of chromosomes in an organism's cells.

## Single nucleotide polymorphisms (SNP)

Single nucleotide polymorphisms, frequently called SNPs (pronounced “snips”), are the most common type of genetic variation among people. Each SNP represents a difference in a single DNA building block, called a nucleotide. For example, a SNP may replace the nucleotide cytosine (C) with the nucleotide thymine (T) in a certain stretch of DNA.

## Haplotype

A haplotype (haploid genotype) is a group of alleles in an organism that are inherited together from a single parent.

## Phased data

Phased data are ordered along one chromosome and so from these data you know the haplotype.

## Unphased data

Unphased data are simply the genotypes without regard to which one of the pair of chromosomes holds that allele.

## cDNA

cDNA (short for copy DNA; also called complementary DNA) is synthetic DNA that has been transcribed from a specific mRNA through a reaction using the enzyme reverse transcriptase.

## DNA microarray

A DNA microarray is a collection of microscopic DNA spots attached to a solid surface. Scientists use DNA microarrays to measure the expression levels of large numbers of genes simultaneously.

## k-mer

In bioinformatics, k-mers are substrings of length k contained within a biological sequence.

## RNA-seq

RNA-Seq (RNA sequencing) is a technique that uses next-generation sequencing (NGS) to reveal the presence and quantity of RNA molecules in a biological sample, providing a snapshot of gene expression in the sample, also known as transcriptome.

## Ribosome

Ribosomes are macromolecular machines, found within all cells, that perform biological protein synthesis (messenger RNA translation). Ribosomal RNA is found in the ribosomal nucleus where this synthesis happens. Ribosomes link amino acids together in the order specified by the codons of messenger RNA molecules to form polypeptide chains.

## Scaffold

Sequences separated by gaps of known length.

## PCR (Polymerase Chain Reaction)

PCR is a method widely used to make millions to billions of copies of a specific DNA sample rapidly, allowing scientists to amplify a very small sample of DNA (or a part of it) sufficiently to enable detailed study.

## Primer

A primer is a short single-stranded nucleic acid used by all living organisms in the initiation of DNA synthesis.

## OTU (Operational taxonomic unit)

OTUs are cluster of similar sequence variants of the 16S rDNA marker gene sequence. Each of these cluster is intended to represent a taxonomic unit of a bacteria species or genus depending on the sequence similarity threshold. Typically, OTU cluster are defined by a 97% identity threshold of the 16S gene sequences to distinguish bacteria at the genus level.

## Operon

An operon is a functioning unit of DNA containing a cluster of genes under the control of a single promoter.

## mRNA

Messenger RNA (abbreviated mRNA) is a type of single-stranded RNA involved in protein synthesis.

## Open Reading Frame

In molecular biology, open reading frames (ORFs) are defined as spans of DNA sequence between the start and stop codons.

## Alternative splicing

Alternative splicing is a cellular process in which exons from the same gene are joined in different combinations, leading to different, but related, mRNA transcripts. These mRNAs can be translated to produce different proteins with distinct structures and functions — all from a single gene.

## Untranslated Region

An untranslated region (or UTR) refers to either of two sections, one on each side of a coding sequence on a strand of mRNA. If it is found on the 5' side, it is called the 5' UTR (or leader sequence), or if it is found on the 3' side, it is called the 3' UTR (or trailer sequence).

## Codon Usage Bias

Codon usage bias refers to differences in the frequency of occurrence of synonymous codons in coding DNA.

## Sense

In molecular biology and genetics, the sense of a nucleic acid molecule, particularly of a strand of DNA or RNA, refers to the nature of the roles of the strand and its complement in specifying a sequence of amino acids.

## Reading Frame

In molecular biology, a reading frame is a way of dividing the sequence of nucleotides in a nucleic acid (DNA or RNA) molecule into a set of consecutive, non-overlapping triplets. In general, at the most, one reading frame in a given section of a nucleic acid, is biologically relevant (open reading frame).

## tRNA

Transfer RNA (abbreviated tRNA) is an adaptor molecule composed of RNA, typically 76 to 90 nucleotides in length (in eukaryotes), that serves as the physical link between the mRNA and the amino acid sequence of proteins. Transfer RNA (tRNA) does this by carrying an amino acid to the protein-synthesizing machinery of a cell called the ribosome.

## Ribosome-binding site

A ribosome binding site, or ribosomal binding site (RBS), is a sequence of nucleotides upstream of the start codon of an mRNA transcript that is responsible for the recruitment of a ribosome during the initiation of translation.

## Putative gene

A putative gene is a segment of DNA that is believed to be a gene.

## Organelle

In cell biology, an organelle is a specialized subunit, usually within a cell, that has a specific function.

## Genotype

An individual's genotype is the combination of alleles that they possess for a specific gene.

## Transcrition factor

In molecular biology, a transcription factor (TF) is a protein that controls the rate of transcription of genetic information from DNA to messenger RNA, by binding to a specific DNA sequence. The function of TFs is to regulate (turn on and off) genes in order to make sure that they are expressed in the desired cells at the right time and in the right amount throughout the life of the cell and the organism.