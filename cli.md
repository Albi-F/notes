# Command-line interface

## AWK

In awk, columns are called fields and rows are called records

- ` ` --> default field separator (this has a special meaning, it denotes any number of spaces or tabs)
- `\n` --> default record separator

### Built-in variables

These variables can be set using the command line option `-v`

Input:

- `FS` --> input field separator (input, and output too if OFS not specified)
- `RS` --> input record separator (input, and output too if ORS not specified)

Output:

- `OFS` --> output field separator
- `ORS` --> output record separator

Counts:

- `NR` --> keeps a current count of the number of input records (across all files)
- `FNR` --> keeps a current count of the number of input records (reset with every new file)
- `NF` --> keeps a count of the number of fields in an input record (can be used to access the last field)

Others:

- `FILENAME` --> contains the name of the current input file

### Field access

- `$0` --> stores the entire record
- `$1`, `$2`, ... --> store the respective field

### Awk substr

`substr(s, a, b)` --> returns b number of chars from string s, starting at position a

### Examples

#### FASTA Files

**Frequency table of start codons**
```bash
awk -vRS=">" -vFS="\n" '{start_codon=substr($2, 0, 3); dict[start_codon]++} END {for (key in dict)  print key, dict[key]}' input_file.fna
```

**Calculate the GC content**
```bash
awk '!/^>/{gc+=gsub(/[gGcC]/,""); at+=gsub(/[aAtT]/,"");} END{ printf "%.2f%%\n", (gc*100)/(gc+at) }' input_file.fna
```

**Calculate the number of sequences, the length of every sequence, the total length and the average length**
```bash
awk -vRS=">" -vFS="\n" 'BEGIN {num_seq=0; tot_len=0} // {print $1; num_seq+=1; seq_len=0; for(i=2; i<NF; i++) seq_len+=length($i); print "Sequence lenght:", seq_len, "\n"; tot_len+=seq_len} END {print "### INFO ###", "\nNumber of sequences:", num_seq, "\nTotal length:", tot_len, "\nAverage length:", tot_len/num_seq, "\n"}' input_file.fasta
```

**Calculate the number of headers in a fasta file**
```bash
awk 'BEGIN {num_head=0} /^>/ {num_head+=1} END {print "Number of headers:", num_head}' input_fasta.fasta
```

**Linearize a fasta file (all nucleotides on the same line)**
```bash
awk '/^>/ {printf("\n%s\n",$0);next; } { printf("%s",$0);}  END {printf("\n");}' input_file.fasta > output_file.fasta
```

**Keep only the id in the header**
```bash
awk '/^>/ {print $1; next} { print $0}' input.faa  > output.faa
```

**Use awk with two files**
```bash
awk 'NR==FNR {if (match($0, "***** No hits found *****")) {no_hit_blastp[$1]; next}} $1 in no_hit_blastp {hit_pfam[$1]} END {print "No hit with blastp:", length(no_hit_blastp), "\nHit in pfam but not in blastp", length(hit_pfam)}' RS="Query= " FS="\n"  pi.blastp RS="\n" FS=" " pi.pfam
```
- awk iterates over the files one a time, starting from the first line on the command line
- The condition `NR==FNR` matches only the first file
- The next is used to move to the second file
- It is possible to set different field and record separators for different files

**Look if a specific string matches a field**
```bash
awk 'BEGIN {counter=0} /^Paxin1_/ {if ($2=="SP(Sec/SPI)") {counter+=1}} END {print "Number of secret proteins:", counter}' 143_summary.signalp5
```

**Read values related to the same sequence ID from two files**
```bash
awk -vOFS="\t" 'NR==FNR && /^Paxin1_/ {if ($2=="SP(Sec/SPI)") {sec_prot[$1]=$3;}; next} /^Paxin1_/ && $1 in sec_prot {sec_prot[$1]=sec_prot[$1]"\t"$4} END {print "Sequence\tsignalp\t\ttargetp"; for (key in sec_prot) print key"\t" sec_prot[key]}' 143_summary.signalp5 143_summary.targetp2 
```

#### FASTQ

**Calculate the number of reads and average read length**
```bash
awk 'BEGIN {num_reads=0; tot_seq_len=0} /^[ACGTN]+$/ {num_reads+=1; total_seq_len+=length($0)} END {print "Number of reads:", num_reads, "\nAverage read length:", total_seq_len/num_reads}' input_fastq.fastq
```

#### GFF Files

**Find the exons in a gtf file**
```bash
awk 'BEGIN{num_genes=0} $3=="gene" {num_genes+=1} $3=="CDS" {num_CDS+=1} END {print "Number of Genes:", num_genes, "\nNumber of exons:", num_CDS, "\nAverage exons for gene:", num_CDS/num_genes} ' input_file.gff
```

**Find the average intron length**
```bash
awk 'BEGIN {num_introns=0; introns_size=0} $3=="intron" {num_introns+=1; introns_size+=$5-$4} END {print "Number of Introns:", num_introns, "\nTotal introns length:", introns_size, "\nAverage introns size:", introns_size/num_introns}' input_file.gff
```

**Count the number of scaffolds**
```bash
awk '{scaffolds[$1]} END {for (key in scaffolds) print key"\t" scaffold[key]; print "\nTotal number of scaffolds:\t", length(scaffolds)}' genes.gff
```

#### VCF

**Count lines excluding metadata**
```bash
awk 'BEGIN {sites=0} !/^#/ {sites+=1} END {print "Sites:", sites}' variants.vcf
```

```bash
awk -vFS="\t" -vOFS="\t" '{if ($8 >= 0.8 && $3 == "Bacteria") {print substr($1, 0, index($1, ";")-1), $9}}' all.fixedRank > filteredAll.fixedRank
```


## update_blastdb.pl (TO FIX!!)

```bash
 perl `which update_blastdb.pl`
```


## Basename

Strip directory and, optionally, the suffix from filenames.

**Return the name of a text file without directory and extension**
```bash
basename '/home/user/my documents/report.txt' .txt
```

**Use `basename` inside a loop** \
  Rename all files ending with “.jpeg” in the current directory by replacing the file extension from “.jpeg” to “.jpg”
```bash
for file in *.jpeg; do
    mv -- "$file" "$(basename $file .jpeg).jpg"
done
```


## Bc (basic calculator)

You can use it in interactive mode

Or with echo:

```bash
echo "scale=1; 15/2" | bc
```


## Cat

- `-n` --> number all output lines


## Column

It is used to display the contents of a file in columns. Empty lines from the input are ignored unless the `-e` option is used.

- `-t` --> Applied for creating a table by determining the number of columns.
- `-R <columns>` --> right align text in these columns
- `-s <string>` --> possible input item delimiters (default is whitespace)
- `-o <string>` --> columns delimiter for table output (default is two spaces)

### Examples

**Watch tab-separated file with less**
```bash
column -ts $'\t' all.fixedRank | less -S -#10
```

### Aliases

```bash
tsv(){
    if test -f "$1"
    then
        column -ts $'\t' "$1" | less -S -#10;
    else
        echo "No such file"
    fi
}
```
It can be added to `.bashrc`. Do not use the other quotes (") for the tab character!

## Cut

It allows to cut line parts from specified files or piped data.

- `-f <list>` - select by specifying a field, a set of fields, or a range of fields
- `-d` --> delimiter that will be used instead of the default “TAB” delimiter
- `--complement` --> displays all bytes, characters, or fields except the selected ones
- `--output-delimiter` --> by default cut uses the input delimiter as the output delimiter, with this option it is possible to specify a different output delimiter string


## Diff

This command is used to display the differences in the files by comparing the files line by line.

## Du

The `du` command in Linux is used to estimate file and directory space usage.

### Flags

- `-d <number>` --> max depth (0 means only top folder/s)
- `-h` -->human readable output

## Grep

`grep` searches the named input FILEs (or standard input if no files are named) for lines containing a match to the given PATTERN. By default, `grep` prints the matching lines.

```bash
grep [OPTIONS] PATTERN [FILE...]
```

### Syntax

#### Matching Control

- `-v` --> invert the sense of matching, to select non-matching lines
- `-i` --> ignore case
- `-w` --> select only those lines containing matches that form whole words
- `-x` --> select only those matches that exactly match the whole line

#### General Output Control

- `-c` --> count number of occurences
- `-m` --> stop reading a file after NUM matching lines

#### Context Line Control

- `-B <NUM>` --> print NUM lines of leading context
- `-A <NUM>` --> print NUM lines of trailing context
- `-C <NUM>` --> print NUM lines of leading and trailing context

### Examples

**Output the line with the text string in it plus the subsequent 20 lines**
```bash
cat trna.fna | grep -A 20 "tRNA anticodon frequency"
```


## Head

- `n [-]NUM` --> output the first NUM lines instead of the first 10; with the leading '-', print all but the last NUM lines of each file


## Locate

The `locate` command is a tool used to find files by their name.

- `updatedb` --> this command updates the database that the `locate` command uses for its searches

### Flags

- `-i` --> ignores case sensitivity
- `-l` --> limits the number of outputs

## Ln

`ln` is a command-line utility for creating links between files. By default, the `ln` command creates hard links.

The `ln` command syntax for creating symbolic links is as follows:

```bash
ln -s [OPTIONS] TARGET LINK_NAME
```

### Parameters

- `-s` --> symbolic link
- `-r` --> resolve relative paths

### Examples

**Create a symbolic link in the current directory**
```bash
ln -s ../path/to/file.txt .
```

## Mkdir

It is used to create directories.

### Flags

- `-p` --> it creates nested directories if they don't exist already


## Nohup

Nohup, short for no hang up is a command in Linux systems that keep processes running even after exiting the shell or terminal. Nohup prevents the processes or jobs from receiving the `SIGHUP` (Signal Hang UP) signal. This is a signal that is sent to a process upon closing or exiting the terminal.


## Paste

`paste` is a command that allows you to merge lines of files horizontally. It outputs lines consisting of the sequentially corresponding lines of each file specified as an argument, separated by tabs.


## Sed

SED is a text stream editor used on Unix systems to edit files quickly and efficiently. The tool searches through, replaces, adds, and deletes lines in a text file without opening the file in a text editor.

### Substitution

`s/ regexp / replacement / flags`


## Sleep

This command pauses the execution for the amount of time defined by the parameter.

### Examples

**Pause execution for 3 minutes**
```bash
sleep 3m
```


## Sort

The `sort` command is a Linux program used for printing lines of input text files and concatenation of all files in sorted order. Sort command takes blank space as field separator and the entire input file as the sort key. It is important to note that `sort` does not actually sort the files but only prints the sorted output.

- `-u` --> with `-c`, check for strict ordering; without `-c` outputs only the first of an equal run
- `-t=<SEP>` --> use SEP instead of non-blank to blank transition
- `-k=<KEYDEF>` --> sort via a key; KEYDEF gives location and type
- `-c` --> check for sorted input; do not sort

## Ssh

SSH client) is a program for logging into a remote machine and for executing commands on a remote machine.  It is intended to provide secure encrypted communications between two untrusted hosts over an insecure network.

- `-T` --> disable pseudo-terminal allocation

## Stty

On Unix-like operating systems, the `stty` command changes and prints terminal line settings.


## Tail

Tail is a command which prints the last few number of lines (10 lines by default) of a certain file, then terminates.

- `n [+]NUM` --> output the last NUM lines or use -n +NUM to output starting with line NUM
- `-f` --> output appended data as the file grows


## Tee

It reads the standard input and writes it to both the standard output and one or more files. The command is named after the T-splitter used in plumbing.

- `-a` --> append to the given file

## Uniq

`uniq` filters out the adjacent matching lines from the input file (that is required as an argument) and writes the filtered data to the output file.

- `-c` –-> it tells how many times a line was repeated by displaying a number as a prefix with the line
- `-d` –-> it only prints the repeated lines and not the lines which aren’t repeated
- `-u` –-> it allows you to print only unique lines

## Wc (word count)

`wc` is used to find out the number of newline count, word count, byte and character count in one or more files.

- `-l` --> print the number of lines
- `-w` --> print the number of words
- `-m` --> print the number of characters
- `-c` --> print the number of bytes
- `-L` --> prints the length of the longest line in a file