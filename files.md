# Files

## Archives

**zip**

ZIP is an archive file format that supports lossless data compression.

```bash
unzip -d ./output_directory archive.zip
```

**tar.gz**

A `.tar.gz` or `.tgz` file is a compressed archive file format that is a combination of two other archive formats, namely TAR (tape archive) and GZIP. The TAR format is used to store multiple files into one archive, while GZIP is used to compress the data in the archive to reduce its size.

```bash
tar -xvzf archive.tar.gz
```

**gz**

```bash
gunzip -k (keep file) archive.gz
```

**bzipped2**

```bash
tar -xvjf archive.tar.bz2
```

### Tar

- `-x` --> extract files from an archive
- `-v` --> verbose
- `-z` --> filter the archive through gzip
- `-f` --> use a file instead of stdin
- `-j` --> read or write archives through bzip2


## Bioinformatics

### FASTA

FASTA format holds a nucleotide or amino acid sequences, following a (unique) identifier, called a description line.
The description line precedes the sequence. It starts with a `>` followed by the sequence identifier and is delimited within a single line for each sequence.

#### Extensions

- `.fasta`, `.fas`, `.fa`--> generic
- `.fna` --> fasta files containing nucleotides
- `.faa` --> fasta files containing amino acids

### FASTQ

Fastq groups together sequence and its quality scores. In fastq files each entry is associated with 4 lines:

- Line 1 begins with a `@` character and is a sequence identifier and an optional description.
- Line 2 has the sequence in standard one letter code.
- Line 3 begins with a `+` character and is optionally followed by the same sequence identifier (and any additional description) again.
- Line 4 encodes the quality values for the sequence in line 2, and must contain the same number of symbols as letters in the sequence.

#### Extension

- `.fastq`, `.fq`

### GFF (General Feature Format)

GFF files are used for describing genes and other features of DNA, RNA and protein sequences.

### SAM (Sequence Alignment Map)

SAM files  contain the alignment information of various sequences that are mapped against reference sequences.

#### BAM (Binary Alignment Map)

BAM files are the binary equivalent of SAM files, they store the same data in a compressed binary representation.