# Gnome

GNOME is a free and open-source desktop environment for Linux and other Unix-like operating systems.

`Alt + Tab` --> switch between different programs
`Alt + §` --> switch between instances of the same application
`Ctrl + Alt + Left/Right` --> change virtual desktop