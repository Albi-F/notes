# Programs

## Conda/Mamba

Conda is an open-source, cross-platform, language-agnostic package manager and environment management system.

### Environments

Create an environment

```bash
mamba create -n env_name
```

Activate an environment

```bash
mamba activate env_name
```

Deactivate the current environment

```bash
mamba deactivate
```

List all environments

```bash
mamba env list
```

### Channels

Shows configured channels

```bash
conda config --show channels
```

Add a new channel and make it the highest priority

```bash
conda config --add channels new_channel
```

### Softwares

Install

```bash
mamba install -c channel program=x.x.x
```

List installed

```bash
conda list
```

Search

```bash
conda search <program>
```

Find in which environment a program is installed

```bash
PROGRAM=prog_name; locate $PROGRAM ~ | grep "/bin/$PROGRAM"
```

## Docker

Docker is a platform designed to help developers build, share, and run container applications.

### Containers

Start a container

```bash
docker start my_container
```

Stop a container

```bash
docker stop my_container
```

Open a terminal inside a container

```bash
docker exec -it my_container bash
```

- You can only mount a volume when you create a container from an image
- `docker run` creates a container
- `docker exec` executes a container

### Storage

Copy files to/from a container

```bash
docker cp file.txt my_container:/path/to/folder/
docker cp my_container:/path/to/folder/file.txt .
```

Bind mounts

A bind mount is used to mount a file or directory on the host machine into a container.

```bash
docker run --mount type=bind,src=<host-path>,dst=<container-path>
```

## Flatpak

Open program

```bash
flatpak run org.chromium.Chromium multiqc_report.html
```

List installed programs

```bash
flatpak list
```

## Snakemake

- `-p` --> print out the shell commands that will be executed
- `-j` --> use at most N CPU cluster/cloud jobs in parallel, for local execution this is a preferred alias called `–cores`
- `-n` --> do not execute anything, and display what would be done (dry-run)
- `--use-conda` --> if defined in the rule, run job in a conda environment. If this flag is not set, the conda directive is ignored

## Systemd

### Units

A unit is a systemd object that performs or controls a particular task or action. Systemd uses units to start/stop/manage services, organize boot process, maintain tasks and processes, create sockets, mount file-system and initialize hardware. A systemd unit consists of a name, type, and configuration file.

The type of each unit can be inferred from the suffix on the end of the file.

#### Services

A unit configuration file whose name ends in ".service" encodes information about a process controlled and supervised by systemd.

These commands are useful for starting or stopping services during the current session:

Start

```bash
sudo systemctl start <application>.service
```

Stop

```bash
sudo systemctl stop <application>.service
```

Restart

```bash
sudo systemctl restart <application>.service
```

To tell systemd to start services automatically at boot, you must enable them:

Enable

```bash
sudo systemctl enable application.service
```

Disable

```bash
sudo systemctl disable application.service
```

This will provide you with the service state, the cgroup hierarchy, and the first few log lines:

Check status of the service

```bash
systemctl status application.service
```
