# Text-based user interfaces

## Less

### Search

- `/` --> search something in the text file
- `n` --> next match
- `N` --> previous match
- `p` --> first match

Browse the search history with the arrow keys!

### Others

- `g` --> go to top
- `G` --> go to bottom

- `-S` --> lines longer than the screen width are chopped (you can see the rest by scrolling horizontally).
  - `-#` --> set the amount of horizontal scrolling
- `-N` --> display line numbers
- `! shell-command` --> invokes a shell to run the shell-command given; `%` in the command is replaced by the name of the current file

## Nano

Nano is an easy-to-use command line text editor.
It has a configuration file called `.nanorc` in the user home folder.

- `Alt + U` --> undo
- `Alt + E` --> redo

- `Alt + .` --> go to the next opened file
- `Alt + ,` --> go to previous opened file

## Neovim

Neovim is a free and open-source, screen-based text editor program.

### Modes

- **Normal Mode**: This is the "eagle-eye" mode that allows you to quickly navigate through your code, delete, edit, copy, and paste easily. If you start the editor you are in this mode.
- **Insert Mode**: This mode is where you write your text or code. It may seem slow and clunky, but getting the job done is necessary.
- **Command-Line Mode**: This enables you to run commands within the editor, such as searching and replacing text.
- **Visual Mode**: Allows you to select text and perform various actions. The possibilities are endless and are covered in depth in the documentation.

### Command mode

- `q` --> quit when the file has not been edited
- `q!` --> quit without saving
- `w` --> save edits
- `term` --> open the terminal

## Screen

Screen is a terminal multiplexer. It means that it can be used to start a screen session and then open any number of windows (virtual terminals) inside that session. Processes running in Screen will continue to run when their window is not visible even if the user disconnect.

## Tmux

Tmux is a terminal multiplexer. It lets you switch easily between several programs in one terminal, detach them (they keep running in the background) and reattach them to a different terminal.

### Commands

**All** commands in tmux are triggered by a **prefix key** followed by a **command key**.
By default, tmux uses `C-b` as prefix key.

- `C-` means "press and hold the Ctrl key". 
- `M-` means "press and hold the Meta key (usually Alt)".

Thus `C-b` means press the Ctrl and b keys at the same time.


#### Sessions

**Create**

- `tmux` --> create a new session, the name will be a number
- `tmux new -s <name>` --> create a session named `<name>`

**Close**

- `tmux kill-session -t <name>` --> from outside tmux
- `:kill-session` --> from within tmux

**Detach**

`d` --> detach the current session

**List**

`tmux ls` --> list the currently running sessions

**Attach**

`tmux attach -t <name>` --> attach to the session names `<name>`

**Rename**

`tmux rename-session -t <name> <new_name>` --> rename the session `<name>` to `<new_name>`


#### Windows

They are like virtual desktops.

**Create**

`c` --> the new windows will be added to the status bar

**Navigation**

- `p` --> go to the previous window
- `n` --> go to the next window
- `<num>` --> go to that specific window


#### Panes

**Splitting**

- Horizontal split --> `"`
- Vertical split --> `%`

**Navigation**

- Use the arrows to move to the other panes

**Exit**

Type `exit` as when closing a regular terminal session.

**Fullscreen**

`z` --> toggle a specific pane fullscreen on/off
